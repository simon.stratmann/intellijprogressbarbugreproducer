package de.sist.gitlab.pipelinemonitor;

import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.progress.impl.BackgroundableProcessIndicator;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.startup.StartupActivity;
import com.intellij.util.concurrency.AppExecutorUtil;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

public class ProgressBarReproductionService implements StartupActivity {


    public void reproduceBackgroundProgressBarBug(Project project) {
        Task.Backgroundable shortRunningTask = new Task.Backgroundable(project, "This progress bar doesn't go away", false) {
            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                System.out.println("Doesn't go away");
            }
        };
        ProgressManager.getInstance().runProcessWithProgressAsynchronously(shortRunningTask, new BackgroundableProcessIndicator(shortRunningTask));

        Task.Backgroundable longRunningTask = new Task.Backgroundable(project, "This progress bar does go away", false) {
            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                try {
                    System.out.println("Goes away");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        ProgressManager.getInstance().runProcessWithProgressAsynchronously(longRunningTask, new BackgroundableProcessIndicator(shortRunningTask));
    }


    @Override
    public void runActivity(@NotNull Project project) {
        AppExecutorUtil.getAppScheduledExecutorService().scheduleWithFixedDelay(() -> reproduceBackgroundProgressBarBug(project), 0, 3, TimeUnit.SECONDS);
    }
}
